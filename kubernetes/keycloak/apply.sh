#!/bin/sh

kubectl apply -f keycloak/deployment.yml
kubectl apply -f keycloak/service.yml
kubectl apply -f keycloak/ingress.yml

