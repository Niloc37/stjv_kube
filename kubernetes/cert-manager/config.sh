#!/bin/sh

kubectl create namespace cert-manager

#Create CRD like ClusterIssuer
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v0.14.1/cert-manager.crds.yaml

kubectl apply -f cert-manager/cluster-issuer.yaml

helm repo add jetstack https://charts.jetstack.io
helm install cert-manager --namespace cert-manager jetstack/cert-manager --version v0.14.1
