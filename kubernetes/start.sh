#!/bin/sh

export KUBECONFIG=./kubeconfig.yml

echo ------------------------------------------
kubectl cluster-info
echo ------------------------------------------
read -p "Do you want to continue ? [y/n] " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi

./ingress_controller/install.sh
./keycloak/apply.sh
./cert-manager/conf.sh
./dashboard/apply.sh


